var w = window;
var d = document;

function parseBool(val) {
  switch(typeof(val)) {
    case "number":
      if(val === 1) return true;
      return false
    case "string":
      if(val.toLowerCase() === "true" || val === "1") return true;
      return false;
    case "boolean":
      return val;
    default:
      return void 0;
  }
}

if (!String.format) {
  String.format = function(format) {
    var args = Array.prototype.slice.call(arguments, 1);
    return format.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

var GMConfig = {
  rgx : {
    KEYCODE : /^[a-zA-Z0-9\-]$/i,
    ORDER : /^([0-9]{2}-[0-9]{8})$/i
  },
  notificationMode : (sessionStorage.notificationMode)?parseBool(sessionStorage.notificationMode):false,
  sendSMS : (sessionStorage.sendSMS)?parseBool(sessionStorage.sendSMS):true,
  sendEmail : (sessionStorage.sendEmail)?parseBool(sessionStorage.sendEmail):true,
  smsOverride : (sessionStorage.smsOverride)?parseBool(sessionStorage.smsOverride):false,
  smsMessage : (sessionStorage.smsMessage)?sessionStorage.smsMessage:"",
  barcodeResetDelay : 2000
};

window.dev = {};

var gmmode = GMConfig.notificationMode;

(function () {
  var isInit = false;

  var toggleButton = d.createElement('button');
  toggleButton.style.marginRight = "5px;"
  toggleButton.classList = "pull-right btn-rex btn-"+(gmmode?"success":"danger");
  toggleButton.innerHTML = '<span class="glyphicon glyphicon-envelope"></span>Notfication Mode: '+(gmmode?"ON":"OFF");
  toggleButton.addEventListener('click', function() {
    GMConfig.notificationMode = gmmode = sessionStorage.notificationMode = !gmmode;
    toggleButton.classList = "pull-right btn-rex btn-"+(gmmode?"success":"danger");
    toggleButton.innerHTML = '<span class="glyphicon glyphicon-envelope"></span>Notfication Mode: '+(gmmode?"ON":"OFF");
    massSendButton.classList = "btn-rex btn-"+(gmmode?"success":"danger");
  });
  d.querySelector('.page-wrap .pull-right').appendChild(toggleButton);

  var smsOverrideEditorHeader = d.createElement('p');
  var smsOverrideEditorMessage = d.createElement('p');
  var smsOverrideEditorFooter = d.createElement('p');
  var smsOverrideEditorButton = d.createElement('button');
  smsOverrideEditorButton.style.marginRight = "5px;"
  smsOverrideEditorButton.classList = 'btn-rex btn-'+(GMConfig.smsOverride?"success":"danger");
  smsOverrideEditorButton.innerHTML = '<span class="glyphicon glyphicon-pencil"></span>Custom SMS Message '+(GMConfig.smsOverride?"ON":"OFF");
  smsOverrideEditorButton.addEventListener('click', function() {
    GMConfig.smsMessage = sessionStorage.smsMessage = prompt("Set your custom SMS message or empty to use default. (Use '|' to specify a new line)", GMConfig.smsMessage.replace(/\n/g, '|')).replace(/\|/g, '\n');
    GMConfig.smsOverride = sessionStorage.smsOverride = (sessionStorage.smsMessage!="");
    smsOverrideEditorMessage.innerHTML = GMConfig.smsMessage.replace(/(\r\n|\n|\r)/g, '<br/>');
    smsOverrideEditorButton.classList = "btn-rex btn-"+(GMConfig.smsOverride?"success":"danger");
    smsOverrideEditorButton.innerHTML = '<span class="glyphicon glyphicon-pencil"></span>Custom SMS Message '+(GMConfig.smsOverride?"ON":"OFF");
  });
  d.querySelector('.page-wrap .pull-right').appendChild(smsOverrideEditorButton);

  var massSendButton = d.createElement('button');
  massSendButton.style.marginRight = "5px;"
  massSendButton.style.display = (document.forms['aspnetForm']['ctl00_ContentPlaceHolder1_FulfilmentTabs_ReportFiltersTab_lbDeliveryMethod'].value == 'Store' && document.forms['aspnetForm']['ctl00_ContentPlaceHolder1_FulfilmentTabs_ReportFiltersTab_txtPLU'].value !== "")?'':'none';
  massSendButton.classList = "btn-rex btn-"+(gmmode?"success":"danger");
  massSendButton.innerHTML = '<span class="glyphicon glyphicon-warning-sign"></span>Mass Send';
  massSendButton.addEventListener('click', function() {
    if(!gmmode) {
      alert('Need to enable notification mode first.');
      return;
    }
    if(confirm(`This will send ${dataTable.getLength()} messages, are you sure?`)) {
      if(confirm(`This DOES NOT CHECK if a customer has already been contacted and will send ${dataTable.getLength()} messages, ARE YOU REALLY SURE YOU WANT TO CONTINUE?`)) {
        massSend();
        return;
      } else {
        alert("Cancelled mass send!");
        return;
      }
      alert("Cancelled mass send!");
      return;
    }
  });
  d.querySelector('.page-wrap .pull-right').appendChild(massSendButton);

  var visibleRows = document.querySelectorAll('#ctl00_ContentPlaceHolder1_FulfilmentTabs_ResultsTab_gvMasterGrid > tbody > tr ~ tr[id]');
  for(var row of visibleRows) {
    var orderID = row.children[1].textContent;
    var actionsColumn = row.children[21];
    var updateRefButton = d.createElement('button');
    updateRefButton.classList = "btn-rex";
    updateRefButton.style = "min-width: 0;"
    updateRefButton.dataset.orderID = orderID;
    updateRefButton.innerHTML = '<span class="glyphicon glyphicon-circle-arrow-up" style="user-events: none;"></span>';
    updateRefButton.addEventListener('click', function(e) {
      e.preventDefault();
      let btn = e.currentTarget;
      updateRef(btn.dataset.orderID).success(function(){
        btn.classList.add('btn-success');
      }).fail(function() {
        btn.classList.add('btn-danger');
      });
      return false;
    });
    actionsColumn.appendChild(updateRefButton);
  }

  if(!GMConfig.smsMessage) { GMConfig.smsOverride = sessionStorage.smsOverride = false; }

  var barcodeString = "";
  var timer = setInterval(barcodeReset, GMConfig.barcodeResetDelay);

  var stylesheet = d.createElement('style');
  stylesheet.textContent = '@keyframes toast-fade { from {opacity:1;} to {opacity:0;} }';
  d.head.appendChild(stylesheet);

  var overlayContainer = d.createElement('div');
  overlayContainer.style = 'position:fixed;top:0;display:flex;width:100%;height:100%;align-items:center;justify-content:center;pointer-events:none;';
  d.body.appendChild(overlayContainer);

  var overlay = d.createElement('div');
  overlay.style = 'background:#000;color:#0f0;font-weight:bold;';
  overlayContainer.appendChild(overlay);

  var toastContainer = d.createElement('div');
  toastContainer.style = 'position:fixed;bottom:40px;right:10px;z-index:1000';
  d.body.appendChild(toastContainer);

  var Toast = function(message, timeout, type){
    var el = d.createElement('div');
    el.close = function() {el.remove();};
    var timer = setTimeout(el.close, timeout);
    el.style = 'font-weight:bold;width:200px;padding:5px;margin:5px;background:#ffa;box-shadow:#000 0 1px 6px -2px;opacity:1;animation:toast-fade 1s ' + (-1+timeout*0.001) + 's forwards;';
    toastContainer.appendChild(el);
    el.innerHTML = message;
    el.addEventListener('click', el.close);
    switch(type){
      case 'ok':
        el.style.background = '#afa';
        break;
      case 'bad':
        el.style.background = '#faa';
        el.style.animation = "";
        clearTimeout(timer);
        break;
      default:
        break;
    }
  };

  w.Toast = Toast;

  w.dataTable = (function(){
    var ready = false;
    var column = [];
    var row = [];

    var init = function (data) {
      if(data instanceof HTMLTableElement) {
        ready = true;

        for(var i = 1; i < data.rows.length; i++) {
          row[i-1] = data.rows[i];
          row[i-1].columns = [];

          for(var j = 0; j < data.rows[i].cells.length; j++) {
            row[i-1].columns[data.rows[0].cells[j].textContent] = data.rows[i].cells[j]; //Column name assoc
          }

          row[row[i-1].columns["OrderNumber"].textContent] = row[i-1]; // OrderID assoc

          row[i-1].getAttribute = function(columnName) {
            return this.columns[columnName].textContent;
          };
        }

      } else {
        console.error("Data not ready");
      }
    };

    var getOrder = function(orderID) {
      return this.row[orderID];
    }

    var getLength = function() {
      return row.length;
    }

    return {
      init : init,
      ready : ready,
      row : row,
      getOrder : getOrder,
      getLength : getLength
    };
  })();

  w.addEventListener('keydown', function(evt){
    if(!gmmode) return;
    if(evt.key == "Enter") {
      if(GMConfig.rgx.ORDER.test(barcodeString)) {
        var email = dataTable.getOrder(barcodeString).getAttribute("Email");
        if(email === "" || email == null || typeof(email) === "undefined") {
          new Toast('Order: ' + barcodeString + '<br>No email address associated with order', 1, 'bad');
        } else {
          sendNotification(dataTable.getOrder(barcodeString));
        }
      }
    } else {
      clearInterval(timer);
      if(GMConfig.rgx.KEYCODE.test(evt.key)) {
        barcodeString += evt.key;
      }
      overlay.textContent = barcodeString;
      timer = setInterval(barcodeReset, GMConfig.barcodeResetDelay);
    }
  });

  w.addEventListener('load', function() {
    main();
  });

  var main = function() {
    isInit = true;

    var optionsContainer  = d.querySelector('#ctl00_ContentPlaceHolder1_FulfilmentTabs_ResultsTab_ResultsContainer tbody');
    if(optionsContainer) {
      optionsContainer.appendChild(d.createElement('tr'));
      optionsContainer.firstElementChild.innerHTML = "<td>SMS Message Override:</td> <td colspan=2 id='GM_SMS_Override_Textarea_Container'></td>";

      smsOverrideEditorHeader.innerHTML = 'Hi {NAME},\nRe: Order XX-XXXXXXXX\n';
      smsOverrideEditorMessage.innerHTML = (GMConfig.smsOverride)?GMConfig.smsMessage.replace(/(\r\n|\n|\r)/g, '<br/>'):'Your order is READY TO COLLECT in store at The Gamesmen.';
      smsOverrideEditorFooter.innerHTML = 'Tel: 0295809888<br/>Directions &amp; Trading Hours: https://www.gamesmen.com.au/location';
      smsOverrideEditorFooter.innerHTML += '<p style="color:red">Please limit the message to as few characters as possible.<br/>Swap "at" for @, "and" for &amp;, shorten words like "competition" to "comp", avoid unncessary punctuation like full-stops</p>';

      //if(GMConfig.smsMessage) {
        d.querySelector('#GM_SMS_Override_Textarea_Container').appendChild(smsOverrideEditorHeader);
        d.querySelector('#GM_SMS_Override_Textarea_Container').appendChild(smsOverrideEditorMessage);
        d.querySelector('#GM_SMS_Override_Textarea_Container').appendChild(smsOverrideEditorFooter);
      //}
    }

    d.getElementById("__EVENTTARGET").value = "ctl00$ContentPlaceHolder1$FulfilmentTabs$ResultsTab$btnExportXLS";
    $.post(theForm.action, $(theForm).serialize()).success(function(data) {
      var parser = new DOMParser();
      if(!w.GM) { w.GM = {}; };
      var vDOM = parser.parseFromString(data, 'text/html');
      dataTable.init(vDOM.body.firstElementChild);
      dataTable.ready = true;
    });

    barcodeReset();
  };

  var barcodeReset = function() {
    barcodeString = "";
    if(gmmode) overlay.textContent = "Waiting for input...";
    else overlay.textContent = "";
  };

  var sendNotification = function(order, callback) {
    if(!gmmode) return;

    let id = order.getAttribute('OrderNumber');
    let addr = order.getAttribute("Email");
    let number = order.getAttribute("Mobile").replace(/[\s-.\(\)]/g, '').replace(/^0/, '+61');
    // REX caps name at 30 characters
    let name = order.getAttribute("CustomerName").replace(/[^a-zA-Z0-9 '!\-]/g, '').replace(/\s+/g, " ").replace(/((The|Mr|Mrs|Ms|Miss|Dr)\s)/, "").trim().split(" ");

    if(number == "" || number == null || typeof number === 'undefined') {
      number = order.getAttribute("Phone").replace(/[\s-.\(\)]/g, '').replace(/^0/, '+61');
    }

    var promises = [];

    var notified = false;

    if(addr === "" || addr == null || typeof(addr) === "undefined") {
      new Toast('['+id+'] No associated email', 0, 'bad');
    } else {
      if(GMConfig.sendEmail) {
        promises.push(
          new Promise((resolve, reject)=>{
            $.ajax({
              type: 'GET',
              url: 'https://staging.gamesmen.com.au/scan/rex_send_email.php',
              data: {
                data : JSON.stringify({
                  "personalizations" : [
                    {
                      "to" : [{ "email" : addr, "name" : name[0]+name[name.length-1] }],
                      "dynamic_template_data" : {
                        "Customer" : {
                          "FirstName" : name[0],
                          "LastName" : name[name.length-1]
                        },
                        "OrderNumber" : id
                      }
                    }
                  ],
                  "from" : {
                    "email" : "sales@gamesmen.com.au",
                    "name" : "The Gamesmen"
                  },
                  "subject" : "The Gamesmen - Order Ready to Collect In-Store",
                  "template_id" : "d-171ee123adfc4cc897b67df8b3a82955"
                })
              }
            }).done(function(){
              resolve(true);
            }).fail(function(){
              reject(false);
            });
          }).then((result)=>{
            new Toast('['+id+'] Email sent to ' + addr, 5000, 'ok');
            return result;
          }).catch((result)=>{
            new Toast('['+id+'] Could not send email to ' + addr, 0, 'bad');
            return result;
          })
        );
      }
    }

    if(GMConfig.sendSMS) {
      if(number == "" || number == null || typeof number === 'undefined') {
        new Toast('['+id+'] No associated mobile', 0, 'bad');
      } else {
        let smsHeader = 'Hi '+name[0]+',\nRe: Order '+id+'\n';
        let smsFooter = '\nTel: 0295809888\nDirections & Trading Hours: https://www.gamesmen.com.au/location';
        let smsMessage = smsHeader + 'Your order is READY TO COLLECT in store at The Gamesmen.' + smsFooter;
        if(GMConfig.smsOverride) {
          smsMessage = smsHeader + GMConfig.smsMessage + smsFooter;
        }
        promises.push(
          new Promise((resolve, reject)=>{
            $.ajax({
              type: 'GET',
              url : 'https://staging.gamesmen.com.au/scan/rex_send_sms.php',
              data: {
                to : number,
                message : encodeURIComponent(smsMessage)
              }
            }).done(function(){
              resolve(true);
            }).fail(function(){
              reject(false);
            });
          }).then((result)=>{
            new Toast('['+id+'] SMS sent to ' + number, 5000, 'ok');
            return result;
          }).catch((result)=>{
            new Toast('['+id+'] Could not send SMS to ' + number, 0, 'bad');
            return result;
          })
        );
      }
    }

    Promise.all(promises).then((values)=>{
      for(let result of values) {
        if(result) {
          if(callback) callback();
          updateRef(id);
          return;
        }
      }
    }).catch((error)=>{
      console.log(error);
    });
  };

  var updateRef = function(id, date) {
    let dt = new Date();
    if(date) dt = new Date(date);
    let reference = String.format("CONTACTED {0}-{1}-{2} {3}-{4}", dt.getFullYear(), ((dt.getMonth()+1>9)?dt.getMonth()+1:'0'+(dt.getMonth()+1)), ((dt.getDate()>9)?dt.getDate():'0'+dt.getDate()), ((dt.getHours()>9)?dt.getHours():'0'+dt.getHours()), ((dt.getMinutes()>9)?dt.getMinutes():'0'+dt.getMinutes()));

    return $.ajax({
      type: 'GET',
      url: 'https://staging.gamesmen.com.au/scan/rex_update_ref.php',
      data: {
        id : id,
        ref : reference
      }
    }).fail(function(){
      new Toast('['+id+'] Error updating reference', 0, 'bad');
    });
  }

  var massSend = function() {
    GMConfig.sendEmail = false;
    var promises = [];
    var ordersNotified = [];
    for(order of dataTable.row){
      var orderID = order.columns["OrderNumber"].textContent;
    	if(!ordersNotified[orderID]) {
        ordersNotified[orderID] = true;
        promises.push(
          new Promise((resolve, reject)=>{
            sendNotification(dataTable.getOrder(orderID), resolve);
          })
        );
      }
    }

    Promise.all(promises).then(()=>{
      console.log(promises);
      alert('Mass send finished!');
    })

    GMConfig.sendEmail = true;
  }

  window.dev.updateRef = updateRef;

  window.dev.showMassUpdate = function() {
    massSendButton.style.display = '';
  }

  var fallbackLoadTicker = setInterval(function() {
    if($.isReady && !isInit) {
      console.log('Fallback Loader Triggered'); clearInterval(fallbackLoadTicker);
      main();
    }
  }, 16);

})();
