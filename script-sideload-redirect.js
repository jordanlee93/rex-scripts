// ==UserScript==
// @name         RetailExpress - Remove iFrame Container
// @match        https://gamesmen.retailexpress.com.au/
// @match        http://gamesmen.retailexpress.com.au/
// @match        https://gamesmen.retailexpress.com.au/DOTNET/Admin/app/
// @match        http://gamesmen.retailexpress.com.au/DOTNET/Admin/app/
// ==/UserScript==
var url = 'https://bitbucket.org/jordanlee93/rex-scripts/raw/master/rex-barcode-redirect.js';

var xhttp;
if ('ActiveXObject' in window) {
  xhttp = new ActiveXObject("Microsoft.XMLHTTP");
} else if (window.XMLHttpRequest) {
  xhttp = new XMLHttpRequest();
} else {
  throw "XHR Not supported";
}

xhttp.onreadystatechange = function () {
  if(xhttp.readyState == 4) {
    if(xhttp.status == 200 || xhttp.status == 0) {
      //HTTP OK or 0 for local fs
      if(xhttp.responseText === "" || xhttp.responseText === null) {
        throw "The request returned an empty respose.";
      } else {
        eval(xhttp.responseText);
      }
    } else {
      throw "The request returned failed with the stauts code " + xhttp.status;
    }
  }
}

xhttp.open("GET", url + '?t=' + new Date().getTime(), true);
xhttp.send();
