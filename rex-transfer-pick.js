(function() {

  var isInit = false;

  window.addEventListener('DOMContentLoaded', main);

  function main() {
    isInit = true;

    var styleSheet = document.createElement('style');
    styleSheet.textContent = `
      .gm-rcv-danger, .gm-rcv-danger td {
        color: white;
        background-color: red;
        font-weight: bold;
      }

      .gm-rcv-warn, .gm-rcv-warn td {
        background-color: orange;
        font-weight: bold;
      }

      .gm-rcv-accept, .gm-rcv-accept td {
        color: white;
        background-color: limegreen;
        font-weight: bold;
      }
    `;
    document.head.appendChild(styleSheet);

    var rows = document.querySelectorAll('.hybrid-table-grid tr[sku]');
    for(row of rows) {
      var textbox = row.querySelector('input');
      var PLU = row.getAttribute('id');

      textbox.setAttribute('plu', PLU);
      textbox.addEventListener('change', evt=>{
        updateColor(evt.target.getAttribute('plu'));
      });

      textbox.setAttribute('readonly', 'true');
      textbox.addEventListener('click', evt=>{
        updateQty(evt);
      });
    }

    _updateCountHelper = updateCountHelper;
    updateCountHelper = function(PLU) {
      var errorLateUpdate = false; // Have to force color change after normal color change
      var expected, current, i;
      for (i in aProductIDs[0]) {
        if (PLU == aProductIDs[0][i] || PLU == aProductIDs[1][i] || PLU == aProductIDs[2][i]) {
          expected = parseInt(aExpectedValues[i]);
          current = parseInt(aTxtInputObjs[i].value);
          aTxtInputObjs[i].scrollIntoView({ behavior : "smooth", block : "center" });

          if (current == expected) errorLateUpdate = true; //overpicked

          // Check if printing mode, don't automatically print for overpicked items, don't print for items with manual quantity updates
          if(typeof silentPrintLabel === "function" && document.querySelector('.pluscan') === document.activeElement && !errorLateUpdate) {
            silentPrintLabel(aProductIDs[0][i]);
          }

          break;
        }
      }

      _updateCountHelper(PLU);

      if(!updateColor(PLU)) {
        (new Audio('//bitbucket.org/jordanlee93/rex-scripts/raw/master/error.mp3')).play();
      }

      if(errorLateUpdate) {
        $(".hybrid-table-grid tbody tr").eq(parseInt(i) + 1).removeClass('gm-rcv-accept gm-rcv-warn gm-rcv-danger highlighted-row success-row error-row');
        $(".hybrid-table-grid tbody tr").eq(parseInt(i) + 1).addClass("gm-rcv-danger");
        (new Audio('//bitbucket.org/jordanlee93/rex-scripts/raw/master/fail.mp3')).play();
        setTimeout(()=>{
          $(".hybrid-table-grid tbody tr input[type=text]").eq(7).focus();
          alert("SCANNED MORE THAN EXPECTED!\n\nPlease confirm that the item count is correct.\n\nThis item will NOT be counted in the transfer!\n\nManually adjust quantity to override limits.");
          $(".hybrid-table-grid tbody tr").eq(parseInt(i) + 1).removeClass('gm-rcv-accept gm-rcv-warn gm-rcv-danger highlighted-row success-row error-row');
          $(".hybrid-table-grid tbody tr").eq(parseInt(i) + 1).addClass("gm-rcv-accept");
        }, 1000);
      }
    }
  };

  function updateQty(evt) {
    newQty = prompt("Enter stock value", evt.target.value);
    if(newQty === null) {
      return;
    }
    if(!isNaN(newQty) && parseInt(newQty) <= parseInt(aMaxQtys[evt.target.getAttribute("index")]) && parseInt(newQty) >= 0) {
      evt.target.value = newQty;
      evt.target.dispatchEvent(new Event('change'));
      document.querySelector('#ctl00_ContentPlaceHolder1_ITOTabs_ReportResultsTab_txtPLUScan').focus();
    } else {
        var sfxError = (new Audio('//bitbucket.org/jordanlee93/rex-scripts/raw/master/error.mp3'));
        sfxError.addEventListener('playing', ()=>{updateQty(evt);});
        sfxError.play();
    }
  }

  function updateColor(PLU) {
    var match = false;
    for (var i in aProductIDs[0]) {
      if (PLU == aProductIDs[0][i] || PLU == aProductIDs[1][i] || PLU == aProductIDs[2][i]) {
        var expected = parseInt(aExpectedValues[i]);
        var current = parseInt(aTxtInputObjs[i].value);
        match = true;

        $(".hybrid-table-grid tbody tr").eq(parseInt(i) + 1).removeClass('gm-rcv-accept gm-rcv-warn gm-rcv-danger highlighted-row success-row error-row');

        if (current > 0 && current < expected) {
          $(".hybrid-table-grid tbody tr").eq(parseInt(i) + 1).addClass("gm-rcv-warn");
        }

        if (current > expected) {
          $(".hybrid-table-grid tbody tr").eq(parseInt(i) + 1).addClass("gm-rcv-danger");
          (new Audio('//bitbucket.org/jordanlee93/rex-scripts/raw/master/fail.mp3')).play();
        }

        if (current == expected) {
          $(".hybrid-table-grid tbody tr").eq(parseInt(i) + 1).addClass("gm-rcv-accept");
        }
      }
    }

    return match;
  }

  if (!String.format) {
    String.format = function(format) {
      var args = Array.prototype.slice.call(arguments, 1);
      return format.replace(/{(\d+)}/g, function(match, number) {
        return typeof args[number] != 'undefined'
          ? args[number]
          : match
        ;
      });
    };
  }

  // Fallback for when the page loads before the event listener can be added
  var fallbackLoadTicker = setInterval(function() {
    if($.isReady && !isInit) {
      console.log('Fallback Loader Triggered'); clearInterval(fallbackLoadTicker);
      main();
    }
  }, 16);

})();
