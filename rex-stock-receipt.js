var GMConfig = {
  propertiesTable : '#results > tbody',
  readyAttribute : 'Season',
  readyValue : 'COMPLETED',
  conditionAttribute : 'Size',
  conditionValue : 'Pre-Owned',
  whitelist : ['Andrew C'],
  managerMode : false,
  defaultFilters : {
    delivery_method : 'Standard', /* Sets default method */
    address: {
        state: '',
    },
    order_date: {
        start : '',
        end : '',
    },
    increment_id : [],
    sku : {
        condition : 'ONLY', /* Applies only to include */
        include : [],
        exclude : [],
    },
    status : '',
    item_count : {
        min : 0,
        max : 50
    },
    order_weight : {
        min : 0,
        max : 5.99
    },
    max_items: 0,
    max_orders: 0
  },
  updateBarcodeForm : {
    "productSelector" : null,
    "newBarcode" : null,
    "skuFieldSelector" : null
  },
  papLoginForm : {
    "username" : null,
    "password" : null
  },
  papCreatePicklistForm : {}
};

(function() {

  var isInit = false;
  var username = "";
  var readyButton = document.createElement('input');
  readyButton.type = 'button';
  readyButton.value = 'Checked for website';
  readyButton.style = 'width:100%;';
  readyButton.classList.add('btn-rex');
  readyButton.addEventListener('click', makeReady);

  window.productTable = {};

  window.addEventListener('DOMContentLoaded', main);

  function main() {
    isInit = true;

    var styleSheet = document.createElement('style');
    styleSheet.textContent = `
      .ross-box td:nth-child(2):after {
        content: 'CHECK WEIGHT & BARCODE';
        color: white;
        background: black;
        font-weight: bold;
        padding: 3px;
        display: block;
        margin: 3px;
      }

      .gm-rcv-danger, .gm-rcv-danger td {
        color: white;
        background-color: red;
        font-weight: bold;
      }

      .gm-rcv-warn, .gm-rcv-warn td {
        background-color: orange;
        font-weight: bold;
      }

      .gm-rcv-accept, .gm-rcv-accept td {
        color: white;
        background-color: limegreen;
        font-weight: bold;
      }

      div.gm-btn-upload-photo {
        position: relative;
        float: right;
        display: inline;
        overflow: visible;
      }

      button.gm-btn-upload-photo {
        position: absolute;
        min-width: auto;
        padding: 3px 5px;
        top: -25px;
        left: -25px;
      }

      button.gm-btn-upload-photo > span.glyphicon {
        margin-right: 0px !important;
      }

      @media print {
        .ross-box {
          color: white;
          background-color: black;
        }
      }
    `;
    document.head.appendChild(styleSheet);

    /* Manager moder button */
    ((e=document.querySelector('.page-wrap'))=>{
      var t = document.createElement('template')
      t.innerHTML =
`<div class="pull-right" style="margin: 10px">
    <button id="btnGMManagerMode" type="button" class="btn-rex btn-danger">
        <span class="glyphicon glyphicon-user"></span>Manager Mode
    </button>
</div>`;
      e.insertBefore(t.content.firstElementChild, e.querySelector('div:first-of-type'));
    })();

    document.querySelector("#btnGMManagerMode").addEventListener('click', (evt)=>{toggleManagerMode();});
    toggleManagerMode(false);

    /* Create picklist button */
    ((e=document.querySelector('.page-wrap'))=>{
      var t = document.createElement('template')
      t.innerHTML =
`<div class="pull-right" style="margin: 10px">
    <button id="btnCreatePicklist" type="button" class="btn-rex btn-primary">
        <span class="glyphicon glyphicon-list-alt"></span>Create Picklist
    </button>
</div>`;
      e.insertBefore(t.content.firstElementChild, e.querySelector('div:first-of-type'));
    })();

    document.querySelector("#btnCreatePicklist").addEventListener('click', (evt)=>{
      if(!(sessionStorage.papSID) || !(sessionStorage.papUsername)) $('#gmPAPLoginModal').modal('show');
      else $('#gmPAPCreatePicklistModal').modal('show');
    });

    /* Update barcode button */
    ((e=document.querySelector('.page-wrap'))=>{
      var t = document.createElement('template')
      t.innerHTML = `
      <div class="pull-right" style="margin: 10px">
          <button id="btnUpdateBarcode" type="button" class="btn-rex btn-primary" data-toggle="modal" data-target="#gmBarcodeModal">
              <span class="glyphicon glyphicon-barcode"></span>Update Barcode
          </button>
      </div>`;
      e.insertBefore(t.content.firstElementChild, e.querySelector('div:first-of-type'));
    })();

    /* Product update modal */
    document.body.appendChild(
      ((e=document.createElement('div'))=>{
        e.classList.add('modal', 'fade', 'in');
        e.id = 'gmBarcodeModal';
        e.innerHTML = `
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button id="gmBarcodeModalClose" type="button" class="close" data-dismiss="modal">
                <span>×</span>
              </button>
              <h2 class="modal-title">Update Product Barcode</h2>
            </div>
            <div class="modal-body">
              <div>
                <label>
                  <span style="display: block; width: 20%;">Product: </span>
                  <select id="gm-product-list"></select>
                </label>
              </div>
              <div>
                <label>
                  <span style="display: block; width: 20%;">Barcode: </span>
                  <input id="gm-new-barcode" type="text" class="txt_std"></input>
                </label>
              </div>
              <div>
                <label>
                  <span style="display: block; width: 20%;">Field: </span>
                  <select id="gm-barcode-field">
                    <option value="sku_s">SKU (Supplier SKU)</option>
                    <!--<option value="sku2">SKU2 (Supplier SKU)</option>--> <!-- REX API doesn't support -->
                    <!--<option value="sku_m">CODE (Manufacturer SKU)</option>--> <!-- REX API requires SupplierSKU to update ManufacturerSKU -->
                  </select>
                </label>
              </div>
            </div>
            <div class="modal-footer">
              <button id="gm-btn-update-barcode" type="button" class="btn-rex">Update</button>
            </div>
          </div>
        </div>`;

        GMConfig.updateBarcodeForm.productSelector = e.querySelector('#gm-product-list');
        GMConfig.updateBarcodeForm.newBarcode = e.querySelector('#gm-new-barcode');
        GMConfig.updateBarcodeForm.skuFieldSelector = e.querySelector('#gm-barcode-field');

        e.querySelector('#gm-btn-update-barcode').addEventListener('click', evt=>{
          updateBarcode(GMConfig.updateBarcodeForm.productSelector.value, GMConfig.updateBarcodeForm.skuFieldSelector.value, GMConfig.updateBarcodeForm.newBarcode.value);
          GMConfig.updateBarcodeForm.productSelector.value = "";
          //GMConfig.updateBarcodeForm.skuFieldSelector.value = "";
          GMConfig.updateBarcodeForm.newBarcode.value = "";
          $('#gmBarcodeModal').modal('hide');
        });

        return e;
      })()
    );

    /* PAP Login modal */
    document.body.appendChild(
      ((e=document.createElement('div'))=>{
        e.classList.add('modal', 'fade', 'in');
        e.id = 'gmPAPLoginModal';
        e.innerHTML = `
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button id="gmPAPLoginModalClose" type="button" class="close" data-dismiss="modal">
                <span>×</span>
              </button>
              <h2 class="modal-title">Pick and Pack Login</h2>
            </div>
            <div class="modal-body">
              <div>
                <label>
                  <span style="display: block; width: 20%;">Username: </span>
                  <input id="gm-pap-username" type="text" class="txt_std"></input>
                </label>
              </div>
              <div>
                <label>
                  <span style="display: block; width: 20%;">Password: </span>
                  <input id="gm-pap-password" type="password" class="txt_std"></input>
                </label>
              </div>
            </div>
            <div class="modal-footer">
              <button id="gm-pap-login" type="button" class="btn-rex">Login</button>
            </div>
          </div>
        </div>`;

        GMConfig.papLoginForm.username = e.querySelector('#gm-pap-username');
        GMConfig.papLoginForm.password = e.querySelector('#gm-pap-password');

        e.querySelector('#gm-pap-login').addEventListener('click', evt=>{
          papLogin(GMConfig.papLoginForm.username.value, GMConfig.papLoginForm.password.value).then((response)=>{
            sessionStorage.papUsername = response.username;
            sessionStorage.papSID = response.session_id;
            $('#gmPAPLoginModal').modal('hide');
            $('#gmPAPCreatePicklistModal').modal('show');
          })
          .catch((err)=>{
            alert(err.message);
          })
        });

        return e;
      })()
    );

    /* PAP Create Picklist modal */
    document.body.appendChild(
      ((e=document.createElement('div'))=>{
        e.classList.add('modal', 'fade', 'in');
        e.id = 'gmPAPCreatePicklistModal';
        e.innerHTML = `
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button id="gmPAPCreatePicklistModalClose" type="button" class="close" data-dismiss="modal">
                <span>×</span>
              </button>
              <h2 class="modal-title">Create Picklist Type</h2>
            </div>
            <div class="modal-body">
              <div>
                <label>
                  <span style="display: block; width: 20%;">Name: </span>
                  <input id="gm-picklist-name" type="text" readonly class="txt_std" value="PO #${$('#POID').val()} (${(new Date()).toJSON().replace(/T.+$/, '')})"></input>
                </label>
              </div>
            </div>
            <div class="modal-footer">
              <button id="gm-btn-create-picklist" type="button" class="btn-rex">Update</button>
            </div>
          </div>
        </div>`;

        GMConfig.papCreatePicklistForm.name = e.querySelector('#gm-picklist-name');

        e.querySelector('#gm-btn-create-picklist').addEventListener('click', evt=>{

          papCreatePicklist(GMConfig.papCreatePicklistForm.name.value).then(()=>{
            GMConfig.papCreatePicklistForm.name.value = `PO #${$('#POID').val()} (${(new Date()).toJSON().replace(/T.+$/, '')})`;
            $('#gmPAPCreatePicklistModal').modal('hide');
          })
          .catch((err)=>{
            alert(err.message);
          });

        });

        return e;
      })()
    );

    var table = document.querySelector(GMConfig.propertiesTable);
    if(table instanceof HTMLTableSectionElement) {
      for(var y = 0; y < table.rows.length; y++) {
        // Grab product list for barcode updating on unknown SKUs
        GMConfig.updateBarcodeForm.productSelector.add(
          ((e=document.createElement('option'))=>{
            e.value = table.rows[y].getAttribute('id');
            e.textContent = table.rows[y].querySelector(".descDiv").textContent.trim().split('\n')[0];
            return e;
          })()
        );

        // Add photo button
        table.rows[y].cells[0].appendChild(
          ((e=document.createElement('div'))=>{
            e.classList.add('gm-btn-upload-photo');
            e.appendChild(
              ((e=document.createElement('button'))=>{
                e.setAttribute('type', 'button');
                e.classList.add('btn-rex', 'gm-btn-upload-photo');
                e.innerHTML = `<span class='glyphicon glyphicon-camera'></span>`;
                e.setAttribute('data-pid', table.rows[y].id);
                e.addEventListener('click', evt=>{
                  var pid = evt.currentTarget.dataset.pid;
                  window.open(`https://gamesmen.retailexpress.com.au/DOTNET/Admin/Inventory/ProductMaint/ImageUpload/ImageUpload.aspx?id=${pid}`, 'Upload Photo', 'height=600,width=1100,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,modal=yes,location=no,directories=no,status=no');
                });
                return e;
              })()
            )
            return e;
          })()
        );

        // Mark for give to web team
        try {
          if(!table.rows[y].cells[2].textContent.includes(String.format('{0}: {1}', GMConfig.conditionAttribute, GMConfig.conditionValue))) {
            if(!table.rows[y].cells[2].textContent.includes(String.format('{0}: {1}', GMConfig.readyAttribute, GMConfig.readyValue))) {
              var row = document.createElement('tr');
              table.rows[y].classList.add('ross-box');
            }
          }
        } catch (ex) {
        }
      }
    }

    productTable = document.querySelector('#results');
    productTable.header = document.querySelector('#results-head');
    //((e=productTable.header.tHead.rows[0].appendChild(document.createElement('th')))=>{e.textContent='Bin';e.style='text-align:center;width:100px;cursor:pointer;';e.addEventListener('click', sortByBinLocation);})();
    productTable.rowByPLU = [];

    for(var i=0;i<aProductIDs[0].length;i++) {
      productTable.rowByPLU[aProductIDs[0][i].toString()] = productTable.rows[i];
    }

    for(row of productTable.rows) {
      ((e=row.appendChild(document.createElement('td')))=>{e.textContent='Loading...';e.style='text-align:center;width:100px;'})();
      row.querySelector('input.inputReceived').addEventListener('change', evt=>{
        var targetRow = evt.composedPath()[2];
        var qty = parseInt(targetRow.querySelector('td[id^="QTY"]').textContent, 10); qty = isNaN(qty)?0:qty;
        var rcv = parseInt(evt.target.value, 10); rcv = isNaN(rcv)?0:rcv;

        productTable.querySelectorAll('tr.highlighted-row, tr.success-row, tr.error-row').forEach(r=>{
          r.classList.remove('gm-rcv-accept', 'gm-rcv-warn', 'gm-rcv-danger', 'highlighted-row', 'success-row', 'error-row');
        });

        targetRow.classList.remove('gm-rcv-accept', 'gm-rcv-warn', 'gm-rcv-danger', 'highlighted-row', 'success-row', 'error-row');

        if(rcv > 0 && rcv < qty) {
          targetRow.classList.add('gm-rcv-warn');
        }

        if(rcv == qty) {
          rcv = qty;
          targetRow.classList.add('gm-rcv-accept');
        }

        if(rcv > qty) {
          //evt.target.value = qty;
          targetRow.classList.add('gm-rcv-danger');
          (new Audio('//bitbucket.org/jordanlee93/rex-scripts/raw/master/fail.mp3')).play();
        }
      });
    }

    var promises = [];
    for(i=0;i<aProductIDs[0].length;i++) {
      promises.push(
        new Promise((resolve, reject)=>{
          fetchBinLocation(i, resolve);
        })
      );
    }
    Promise.all(promises);

    _updateCountHelper = updateCountHelper;
    updateCountHelper = function(PLU) {
      var match = false;
      _updateCountHelper(PLU);
      for(var i in aProductIDs[0]) {
        if(PLU == aProductIDs[0][i] || PLU == aProductIDs[1][i] || PLU == aProductIDs[2][i] || PLU == aProductIDs[3][i]){
          aQtyRcdInputs[i].dispatchEvent(new Event('change'));
          aQtyRcdInputs[i].scrollIntoView({ behavior : "smooth", block : "center" });
          match = true;
        }
      }

      if(!match) {
        (new Audio('//bitbucket.org/jordanlee93/rex-scripts/raw/master/error.mp3')).play();
        $('#gmBarcodeModal').modal('show');
        GMConfig.updateBarcodeForm.newBarcode.value = PLU;
      }
    }
  };

  async function papLogin(username, password) {
    return await fetch("https://pick.gamesmen.com.au/api/user/login", {
      credentials: "same-origin",
      method: "POST",
      body: JSON.stringify({
          username: username,
          password: password
      })
    })
    .then(res => {
        if (res.status !== 200) throw res;
        else return res.json()
    })
    .catch(err => err.json().then(err => { throw err }));
  }

  async function papCreatePicklist(name) {
    let filters = Object.assign({}, GMConfig.defaultFilters);
    filters.sku.include = aProductIDs[0];

    let json = JSON.stringify({filters:filters});

    return await fetch(`https://pick.gamesmen.com.au/api/filter/create?username=${sessionStorage.papUsername}&SID=${sessionStorage.papSID}`, {
      credentials: "same-origin",
      method: "POST",
      body: JSON.stringify({
        name:name,
        json:json
      })
    }).then(res => {
      if (res.status !== 200) throw res;
      else return res.json()
    })
    .catch(err => err.json().then(err => { throw err }));
  }

  function updateBarcode(pid, field, barcode) {

    for(var i in aProductIDs[0]) {
      if(pid == aProductIDs[0][i]){
        if(aProductIDs[1][i] !== "" || aProductIDs[2][i] !== ""|| aProductIDs[3][i] !== "") {
          if(confirm("THIS PRODUCT ALREADY HAS A SKU\n\n"+aProductIDs[1][i]+"\n\n"+"ARE YOU SURE YOU WANT TO OVERWRITE THIS?")) {
            aProductIDs[1][i] = barcode;
            break;
          } else {
            return;
          }
        }
      }
    }

    var options = {};
    options.pid = pid;
    if (field === "sku_s") options.sku_s = barcode;
    if (field === "sku_m") options.sku_m = barcode;
    fetch(new Request('https://pick.gamesmen.com.au/shell/webhook_update_barcode.php?' + new URLSearchParams(options).toString())).then(r=>r.json()).then(rjson=>{
      if(rjson.error) console.error(rjson.error);
    });
  }

  function fetchBinLocation (index, callback) {
    fetch("//gamesmen.retailexpress.com.au/Admin/Inventory/ProductMaint/StockGrid/InventoryGridAjax.asp?drawGrid="+aProductIDs[0][index]).then(r=>r.text()).then(e=>{
        var vDOM = (new DOMParser()).parseFromString(e, 'text/html');
        var inventoryTable = vDOM.querySelector('form[name=myPriceGridForm] table').tBodies[0];
        for(row of inventoryTable.rows) {
            if(row.cells[1].textContent.trim() == "Warehouse") {
                productTable.rowByPLU[aProductIDs[0][index].toString()].lastElementChild.textContent = row.querySelector('input').value;
                productTable.rowByPLU[aProductIDs[0][index].toString()].bin = row.querySelector('input').value;
            }
        };
    }).then(()=>{
      if(callback) callback();
    });
  }

  function toggleManagerMode(enable) {
    if(typeof enable === "undefined") enable = GMConfig.managerMode = !GMConfig.managerMode;
    else GMConfig.managerMode = enable;

    document.querySelector('#btnGMManagerMode').classList.remove('btn-danger', 'btn-primary');
    document.querySelector('#btnGMManagerMode').classList.add(enable ? 'btn-primary':'btn-danger');

    ((e=document.querySelector('table.hybrid-table-grid.remove-margin-bottom').parentElement)=>{
      e.style.position = enable ? '':'fixed';
      e.style.zIndex = enable ? '':'1000';
      e.style.top = enable ? '':'100px';
      e.style.left = enable ? '':'100px';
      e.style.border = enable ? '':'solid black 3px';
    })();

    document.querySelectorAll('div.hybrid-filter-inputs tr:not(:nth-child(1))').forEach(e=>{
      e.style.display = enable ? '':'none';
    });

    document.querySelector('#btnCopyAcross').style.display = enable ? '':'none';
    document.querySelector('table.hybrid-table-grid.remove-margin-bottom tr:nth-child(2)').style.display = enable ? '':'none';
    document.querySelector('table.hybrid-multi-column-form.margin-small.top').style.display = enable ? '':'none';

    document.querySelectorAll('table#results input:not([name^="inptReceived"]), table#results select:not([name^="inptReceived"])').forEach(e=>{
      if(enable) e.removeAttribute('readonly');
      else e.setAttribute('readonly', '');
      e.style.backgroundColor = enable ? '':'lightgrey';
    });

    document.querySelectorAll('input[value="Receive Stock As Available"]').forEach(e=>{
      e.style.display = enable ? '':'none';
    });
  }

  window.binSortAsc = 1;
  function sortByBinLocation () {
    window.binSortAsc *= -1;
    productTable.rowByPLU.sort((a,b)=>{
    	return (a.bin > b.bin) ? window.binSortAsc : -window.binSortAsc;
    });

    while(productTable.rows.length>0) { productTable.rows[productTable.rows.length-1].remove(); }
    productTable.rowByPLU.forEach(r=>productTable.tBodies[0].appendChild(r));
  }

  function makeReady() {
    var readyAttributeElement = document.querySelector(GMConfig.readyAttribute);
    for(o of readyAttributeElement.options) {
      if(o.text === GMConfig.readyValue) {
        readyAttributeElement.selectedIndex = o.index;
      }
    };
  };

  if (!String.format) {
    String.format = function(format) {
      var args = Array.prototype.slice.call(arguments, 1);
      return format.replace(/{(\d+)}/g, function(match, number) {
        return typeof args[number] != 'undefined'
          ? args[number]
          : match
        ;
      });
    };
  }

  // Fallback for when the page loads before the event listener can be added
  var fallbackLoadTicker = setInterval(function() {
    if($.isReady && !isInit) {
      console.log('Fallback Loader Triggered'); clearInterval(fallbackLoadTicker);
      main();
    }
  }, 16);

})();
