var GMConfig = {
    defaultFilters : {
      delivery_method : 'Standard', /* Sets default method */
      address: {
          state: '',
      },
      order_date: {
          start : '',
          end : '',
      },
      increment_id : [],
      sku : {
          condition : 'ONLY', /* Applies only to include */
          include : [],
          exclude : [],
      },
      status : '',
      item_count : {
          min : 0,
          max : 0
      },
      order_weight : {
          min : 0,
          max : 5.99
      },
      max_items: 50,
      max_orders: 0
    },
    papLoginForm : {
      "username" : null,
      "password" : null
    },
    papCreatePicklistForm : {}
  };

(function() {

    var isInit = false;
  
    window.addEventListener('DOMContentLoaded', main);
  
    function main() {
        isInit = true;

        

        /* Create picklist button */
        ((e=document.querySelector('#ctl00_ContentPlaceHolder1_ITOTabs'))=>{
            var t = document.createElement('template')
            t.innerHTML =
`<div class="pull-right" style="margin: 10px">
    <button id="btnCreatePicklist" type="button" class="btn-rex btn-primary">
        <span class="glyphicon glyphicon-list-alt"></span>Create Picklist
    </button>
</div>`;
            e.insertBefore(t.content.firstElementChild, e.querySelector('div:first-of-type'));
        })();
        
        ((e=document.querySelector('#ctl00_ContentPlaceHolder1_ITOTabs'))=>{
            var t = document.createElement('template')
            t.innerHTML =
`<div class="pull-right" style="margin: 10px">
    <button id="btnCopyITO" type="button" class="btn-rex btn-primary">
        <span class="glyphicon glyphicon-list-alt"></span>Copy ITO
    </button>
</div>`;
            e.insertBefore(t.content.firstElementChild, e.querySelector('div:first-of-type'));
        })();
    
        document.querySelector("#btnCreatePicklist").addEventListener('click', (evt)=>{
            if(!(sessionStorage.papSID) || !(sessionStorage.papUsername)) $('#gmPAPLoginModal').modal('show');
            else $('#gmPAPCreatePicklistModal').modal('show');
        });
        
        document.querySelector("#btnCopyITO").addEventListener('click', (evt)=>{
          papCopyITO("subtract");
        });

        /* PAP Login modal */
        document.body.appendChild(
            ((e=document.createElement('div'))=>{
            e.classList.add('modal', 'fade', 'in');
            e.id = 'gmPAPLoginModal';
            e.innerHTML = `
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button id="gmPAPLoginModalClose" type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                    </button>
                    <h2 class="modal-title">Pick and Pack Login</h2>
                </div>
                <div class="modal-body">
                    <div>
                    <label>
                        <span style="display: block; width: 20%;">Username: </span>
                        <input id="gm-pap-username" type="text" class="txt_std"></input>
                    </label>
                    </div>
                    <div>
                    <label>
                        <span style="display: block; width: 20%;">Password: </span>
                        <input id="gm-pap-password" type="password" class="txt_std"></input>
                    </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="gm-pap-login" type="button" class="btn-rex">Login</button>
                </div>
                </div>
            </div>`;
    
            GMConfig.papLoginForm.username = e.querySelector('#gm-pap-username');
            GMConfig.papLoginForm.password = e.querySelector('#gm-pap-password');
    
            e.querySelector('#gm-pap-login').addEventListener('click', evt=>{
                papLogin(GMConfig.papLoginForm.username.value, GMConfig.papLoginForm.password.value).then((response)=>{
                sessionStorage.papUsername = response.username;
                sessionStorage.papSID = response.session_id;
                $('#gmPAPLoginModal').modal('hide');
                    $('#gmPAPCreatePicklistModal').modal('show');
                })
                .catch((err)=>{
                    alert(err.message);
                    })
                });
    
                return e;
            })()
        );
    
        /* PAP Create Picklist modal */
        document.body.appendChild(
            ((e=document.createElement('div'))=>{
            e.classList.add('modal', 'fade', 'in');
            e.id = 'gmPAPCreatePicklistModal';
            e.innerHTML = `
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button id="gmPAPCreatePicklistModalClose" type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                    </button>
                    <h2 class="modal-title">Create Picklist Type</h2>
                </div>
                <div class="modal-body">
                    <div>
                    <label>
                        <span style="display: block; width: 20%;">Name: </span>
                        <input id="gm-picklist-name" type="text" class="txt_std" value="ITO #${$('#ctl00_ContentPlaceHolder1_ITOTabs_EditTab_dvITO_Label1').text()} (${(new Date()).toJSON().replace(/T.+$/, '')})"></input>
                    </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="gm-btn-create-picklist" type="button" class="btn-rex">Update</button>
                </div>
                </div>
            </div>`;
    
            GMConfig.papCreatePicklistForm.name = e.querySelector('#gm-picklist-name');
    
            e.querySelector('#gm-btn-create-picklist').addEventListener('click', evt=>{
                
                papCreatePicklist(GMConfig.papCreatePicklistForm.name.value).then(()=>{
                GMConfig.papCreatePicklistForm.name.value = `ITO #${$('#ctl00_ContentPlaceHolder1_ITOTabs_EditTab_dvITO_Label1').text()} (${(new Date()).toJSON().replace(/T.+$/, '')})`;
                $('#gmPAPCreatePicklistModal').modal('hide');
                })
                .catch((err)=>{
                alert(err.message);
                });
                
            });
    
            return e;
        })()
      );
    }
    
    function papCopyITO(operator) {
      if(!operator) operator = "subtract";
      let products = [];
      
      document.querySelectorAll('#ctl00_ContentPlaceHolder1_ITOTabs_EditTab_gvITOItems tbody tr').forEach(e=>{
          if(e.dataset.productid) {
            let quantity = parseInt(e.querySelector('td.transferAction-Allocated').firstElementChild.textContent);
            
            if(operator == "subtract") quantity = 0 - quantity;
            
            let product = {
              "product_id" : e.dataset.productid,
              "quantity" : quantity
            };
            
            products.push(product);
          }
      });
      
      let json = JSON.stringify({
        products : products
      });
      
      navigator.clipboard.writeText(json).then(()=>{
        alert("ITO copied");
      });
    }

    async function papLogin(username, password) {
        return await fetch("https://pick.gamesmen.com.au/api/user/login", {
          credentials: "same-origin",
          method: "POST",
          body: JSON.stringify({
              username: username,
              password: password
          })
        })
        .then(res => {
            if (res.status !== 200) throw res;
            else return res.json()
        })
        .catch(err => err.json().then(err => { throw err }));
      }
    
      async function papCreatePicklist(name) {
        let ProductIDs = [];
        document.querySelectorAll('#ctl00_ContentPlaceHolder1_ITOTabs_EditTab_gvITOItems tbody tr').forEach(e=>{
            if(e.dataset.productid) ProductIDs.push(e.dataset.productid);
        });

        let filters = Object.assign({}, GMConfig.defaultFilters);
        filters.sku.include = ProductIDs;
    
        let json = JSON.stringify({filters:filters});
    
        return await fetch(`https://pick.gamesmen.com.au/api/filter/create?username=${sessionStorage.papUsername}&SID=${sessionStorage.papSID}`, {
          credentials: "same-origin",
          method: "POST",
          body: JSON.stringify({
            name:name,
            json:json
          })
        }).then(res => {
          if (res.status !== 200) throw res;
          else return res.json()
        })
        .catch(err => err.json().then(err => { throw err }));
      }

      if (!String.format) {
        String.format = function(format) {
          var args = Array.prototype.slice.call(arguments, 1);
          return format.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
              ? args[number]
              : match
            ;
          });
        };
      }
    
      // Fallback for when the page loads before the event listener can be added
      var fallbackLoadTicker = setInterval(function() {
        if($.isReady && !isInit) {
          console.log('Fallback Loader Triggered'); clearInterval(fallbackLoadTicker);
          main();
        }
      }, 16);

})();