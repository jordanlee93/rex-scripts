var imageSelector = document.querySelector('input[type="file"]');
imageSelector.setAttribute('accept', 'image/*;capture=camera');
imageSelector.setAttribute('capture', 'environment');

var player = document.body.appendChild(document.createElement('video'));
player.width = 300;
player.height = 150;
player.style.backgroundColor = '#CCC';
player.setAttribute('autoplay', '');

var canvas = document.body.appendChild(document.createElement('canvas'));
canvas.width = 300;
canvas.height = 150;
canvas.style.display = 'none';

var preview = document.body.appendChild(document.createElement('img'));
preview.height = 150;

document.body.appendChild(document.createElement('br'));

var context = canvas.getContext('2d');
context.fillRect(0,0,3000,3000);

preview.src = canvas.toDataURL('image/png');

var captureButton = document.body.appendChild(document.createElement('button'));
captureButton.textContent = 'Capture';
captureButton.classList.add('btn-rex');
captureButton.style.margin = captureButton.style.padding = '5px';

var uploadButton = document.body.appendChild(document.createElement('button'));
uploadButton.textContent = 'Upload';
uploadButton.disabled=true;
uploadButton.classList.add('btn-rex', 'btn-primary');
uploadButton.style.margin = uploadButton.style.padding = '5px';

const constraints = {};

navigator.mediaDevices.enumerateDevices().then(devices=>{
  for(var device of devices) {
    console.log(device);
    if(device.label.toUpperCase().includes('REAR')) return device.deviceId;
  }
  return null;
}).then((deviceId)=>{
  console.log(`Selected ${deviceId}`);
  if(deviceId !== null) constraints.video = { 'deviceId' : { exact : deviceId } };
  else constraints.video = true;
  return;
}).then(()=>{
  navigator
  .mediaDevices
  .getUserMedia(constraints)
  .then((stream)=>{
    if(typeof stream === 'undefined') return;
    player.srcObject = stream;
  }).catch(ex=>{console.log(ex)});
});

captureButton.addEventListener('click', evt=>{
  evt.preventDefault();
  canvas.width = player.videoWidth ? player.videoWidth : 300;
  canvas.height = player.videoHeight ? player.videoHeight : 150;
  context.drawImage(player, 0, 0);
  setTimeout(()=>{
    preview.src = canvas.toDataURL('image/png')
  }, 500);
  uploadButton.disabled = false;
});

uploadButton.addEventListener('click', evt=>{
  evt.preventDefault();
  canvas.width = 300 ? player.videoWidth : 300;
  canvas.height = 150 ? player.videoHeight : 150;
  canvas.toBlob(uploadImage, 'image/png', 1);
  var formData = new FormData();
  var image = canvas.toDataURL();
  formData.append()
});

function createFileList(a) {
  a = [].slice.call(Array.isArray(a) ? a : arguments)
  for (var c, b = c = a.length, d = !0; b-- && d;) d = a[b] instanceof File
  if (!d) throw new TypeError('expected argument to FileList is File or array of File objects')
  for (b = (new ClipboardEvent('')).clipboardData || new DataTransfer; c--;) b.items.add(a[c])
  return b.files
}

function uploadImage(blob) {
  var form = document.forms[0];
  for(var input of form) {
    if(input.type==='file') {
      //var fileInputName = input.name;
      //input.remove();
      //var pseudoImageUpload = form.appendChild(document.createElement('input'));
      //pseudoImageUpload.type = 'hidden';
      //pseudoImageUpload.name = fileInputName;
      //pseudoImageUpload.value = blob;
      var file = new File([blob], 'blob.png', {type: 'image/png'});
      input.files = createFileList(file);
      break;
    }
  }
  document.querySelector('button[id*=btnUpload]').dispatchEvent(new MouseEvent('click'));
}
