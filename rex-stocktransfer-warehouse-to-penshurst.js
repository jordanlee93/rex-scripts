function parseBool(val) {
  switch(typeof(val)) {
    case "number":
      if(val === 1) return true;
      return false
    case "string":
      if(val.toLowerCase() === "true" || val === "1") return true;
      return false;
    case "boolean":
      return val;
    default:
      return void 0;
  }
}

function Table(htmlElement) {
  var data = htmlElement;
  var ready = false;
  var column = [];
  var row = [];

  var init = function () {
    if(data instanceof HTMLTableElement) {
      ready = true;

      for(var i = 1; i < data.rows.length; i++) {
        row[i-1] = data.rows[i];
        row[i-1].column = [];

        for(var j = 0; j < data.rows[i].cells.length; j++) {
          row[i-1].column[data.rows[0].cells[j].textContent.replace(/[^a-zA-Z0-9]*/g,'')] = data.rows[i].cells[j]; //Column name assoc
        }

        row[i-1].getAttribute = function(columnName) {
          return this.column[columnName].textContent.replace(/[^a-zA-Z0-9\.\-]*/g,'');
        };
      }

    } else {
      console.error("Data not ready");
    }
  };

  init();

  return {
    init : init,
    ready : ready,
    row : row,
  };
};

async function fetchBinLocation (product_id) {
  return await fetch("//gamesmen.retailexpress.com.au/Admin/Inventory/ProductMaint/StockGrid/InventoryGridAjax.asp?drawGrid="+product_id).then(r=>r.text()).then(e=>{
      var vDOM = (new DOMParser()).parseFromString(e, 'text/html');
      var inventoryTable = vDOM.querySelector('form[name=myPriceGridForm] table').tBodies[0];
      for(row of inventoryTable.rows) {
          if(row.cells[1].textContent.includes("Warehouse")) {
              return row.querySelector('input').value;
          }
      };
  })
}

var items = [];

(function() {

  var isInit = false;

  var runButton = document.createElement('button');
  runButton.classList = "btn-rex btn-primary pull-right";
  runButton.innerHTML = '<span class="glyphicon glyphicon-play"></span>Run Calculations';
  runButton.addEventListener('click', function() { sessionStorage.runMacro = true; calculateQty(); });
  document.querySelector('#ctl00_ContentPlaceHolder1_SearchTabs_header').appendChild(runButton);

  var main = function () {
    isInit = true;

    var rawTables = document.querySelectorAll('table.cont');
    var productHeaderTables = document.querySelectorAll("#ctl00_ContentPlaceHolder1_SearchTabs_ResultsTab_MasterGrid table.hybrid-table-grid");
    var productIDs = [];
    
    productHeaderTables.forEach(t => {
        var productID = t.rows[1].cells[1].textContent.trim();

        fetchBinLocation(productID).then(bin_code => {
          t.rows[1].cells[0].innerText += `[BIN: ${bin_code}]`;
        });
    });

    for(i=0;i<rawTables.length;items.push(new Table(rawTables[i++]))){};

    //document.querySelectorAll(`table.cont tbody tr:nth-child(2) input[value="0"]`).forEach(e=>{
      // Have mercy on my soul
      // e.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.style.display = 'none';
    //});

    if(!parseBool(sessionStorage.runMacro)) {
      sessionStorage.preserveStock = window.location.get['gt']?window.location.get['gt']:0;
      sessionStorage.MSL = window.location.get['msl']?window.location.get['msl']:false;
    }

    if(parseBool(sessionStorage.runMacro)) {
      calculateQty();
    }

  }

  window.calculateQty = function () {
    var currentPage, totalPages;

    if(document.getElementById('ctl00_ContentPlaceHolder1_SearchTabs_ResultsTab_TopPager_PageNumber'))
      currentPage = parseInt(document.getElementById('ctl00_ContentPlaceHolder1_SearchTabs_ResultsTab_TopPager_PageNumber').value);
    else
      currentPage = 1;

    if(document.getElementById('ctl00_ContentPlaceHolder1_SearchTabs_ResultsTab_TopPager_PageCount'))
      totalPages = parseInt(document.getElementById('ctl00_ContentPlaceHolder1_SearchTabs_ResultsTab_TopPager_PageCount').textContent);
    else
      totalPages = 1;

    var runningOverlay = document.createElement('div');
    runningOverlay.innerHTML = '<div>Running calculations...</div>';
    runningOverlay.style = 'position:fixed; top:0; left:0; width:100%; height:100%; display:flex; align-items:center; justify-content:center; color:white; background:rgba(0,0,0,.5);';
    document.body.appendChild(runningOverlay);

    if(!parseBool(sessionStorage.initMacro) && totalPages > 1) {
      if(currentPage != 1) {
        document.getElementById('ctl00_ContentPlaceHolder1_SearchTabs_ResultsTab_TopPager_PageNumber').value = 1;
        document.getElementById('ctl00_ContentPlaceHolder1_SearchTabs_ResultsTab_TopPager_PageNumber').dispatchEvent(new Event('change'));
        return;
      } else {
        sessionStorage.initMacro = true;
      }
    }

    // MSL - (Available + Transit-In + Received + Requested)
    for(i=0;i<items.length;i++) {
      var msl = parseBool(sessionStorage.MSL)?parseInt(sessionStorage.MSL):parseInt(items[i].row[0].getAttribute('MSL'));
      var avl = parseInt(items[i].row[0].getAttribute('Avl'));
      var trfin = parseInt(items[i].row[0].getAttribute('TrfIn'));
      var rcd = parseInt(items[i].row[0].getAttribute('Rcd'));
      var req = parseInt(items[i].row[0].getAttribute('Req'));
      var frmavl = parseInt(items[i].row[0].getAttribute('FrmAvl'));
      var preserveStock = parseInt(sessionStorage.preserveStock);

      var qty = msl - (avl + trfin + rcd + req);

      qty=(qty>frmavl)?frmavl:qty;
      qty=(qty>Math.abs(avl) && frmavl-qty<1)?qty-1:qty;
      qty=(qty>0)?qty:0;

      var input = items[i].row[0].column['Qty'].getElementsByTagName('input')[0];
      input.value = qty;
      input.dispatchEvent(new Event('change'));
    }

    // Done with this page, next page...
    if (currentPage < totalPages) {
      // Can't just send click event, must run javascript embedded in the href...
      eval(document.getElementById('ctl00_ContentPlaceHolder1_SearchTabs_ResultsTab_TopPager_NextPage').href.replace('javascript:', ''));
    } else {
      sessionStorage.runMacro = false;
      sessionStorage.initMacro = false;
      alert('Completed calculations');
      runningOverlay.style.display = 'none';
    }
  }

  if(!window.location.get) {
    var a = window.location.search.substr(1).split('&');
    if (a == "") window.location.get={};
    var b = {};
    for (var i = 0; i < a.length; ++i)
    {
      var p=a[i].split('=', 2);
      if (p.length == 1)
        b[p[0]] = "";
      else
        b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    window.location.get = b;
  }

  var fallbackLoadTicker = setInterval(function() {
    if($.isReady && !isInit) {
      console.log('Fallback Loader Triggered'); clearInterval(fallbackLoadTicker);
      main();
    }
  }, 16);

})();
