var printingMode = false;
var STYLE_SHEET;
fetch("//bitbucket.org/jordanlee93/rex-scripts/raw/master/label-stylesheet.css").then(r=>r.text()).then(text=>{ STYLE_SHEET=text; });

window.addEventListener('keydown', (evt)=>{
	if(evt.ctrlKey && evt.code === "KeyP") {
		evt.preventDefault();
		togglePrintingMode();
  }
});

document.querySelector('.page-wrap').insertBefore(
	((e=document.createElement('div'))=>{
		e.classList.add('pull-right');
		e.style.margin = '10px';
		e.innerHTML = `<button id='gm-btn-printing-mode' type='button' class='btn-rex btn-danger'><span class='glyphicon glyphicon-print'></span>Printing Mode</button>`;
		e.addEventListener('click', evt=>{
			togglePrintingMode();
		});
		return e;
	})(),
	document.querySelector('.page-wrap .pull-right')
);

window.togglePrintingMode = function () {
	printingMode = !printingMode;
	((e=document.querySelector('#gm-btn-printing-mode'))=>{
		e.classList.remove('btn-danger', 'btn-primary');
		e.classList.add( (printingMode?'btn-primary':'btn-danger') );
	})();
}

window.silentPrintLabel = function (pid) {
	if(!printingMode) return;

	fetch(new Request(`/Admin/Inventory/StockReceipt/StockReceiptBarcodes/LblSht.asp?pid=${pid}`)).then(r=>r.text()).then(html=>{
		var document = (new DOMParser()).parseFromString(html, 'text/html');
		var str = document.body.innerHTML;
		// Make 2 decimal places if price has cents rounded to nearest 10 (not 0)
		str = str.replace(/<div>(.*)\n(.*)\n(.*\.\d)\n/gi, '<div>$1\n$2\n$30\n');
		// Add 2 decimal places if price has 0 cents
		str = str.replace(/<div>(.*)\n(.*)\n(.*[^\.]\d\d*[^\.])\n/gi, '<div>$1\n$2\n$3.00\n');
		// Add thousands seperator if needed
		str = str.replace(/<div>(.*)\n(.*)\n(.*)(\d)(?=(\d{3})+(?!\d))/g, '<div>$1\n$2\n$3$4,');
		// Add $ to RRP
		str = str.replace(/<div>(.*)\n(.*)\n(.*>)(\d{1,},?\d*\..*)/gi, '<div>$1\n$2\n$3$$$4');
		// If RRP is same as Sell price remove RRP
		str = str.replace(/<div>(.*)\n(.*)\n(.*)\n(\3)/gi, '<div>$1\n$2\n<br />\n$4');
		// Label RRP if it exists and add spacer
		str = str.replace(/<div>(.*)\n(.*)\n(.*)(\$.*)/g, "<div>$1\n$2\n$3RRP:&nbsp;$4");
		str = str.replace(/ \(SerialNum\)/gi, '');
		str = str.replace(/<div>P\/O /gi, '<div class="label preowned"><span>P/O ');
		str = str.replace(/<div>/gi, '<div class="label"><span>');
		str = str.replace(/<\/div>/gi, '</span></div>');
		str = str.replace(/<br\s*[\/]?>/gi, '</span><span>');
		document.body.innerHTML = str;

		((e=document.querySelector('.barcode'))=>{
				e.setAttribute('data-sku', e.textContent.trim());
		})();
		document.querySelector('link').href = document.querySelector('link').href;

		document.head.appendChild(document.createElement('style')).innerHTML = STYLE_SHEET;

		return document.documentElement.outerHTML;
	}).then(html=>{
		fetch(new Request('http://localhost:8008/', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({ 'html' : btoa(html) })
		})).then(r=>r.text()).then(t=>console.log(t));
	});
}
