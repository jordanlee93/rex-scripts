// ==UserScript==
// @name         RetailExpress - Barcode Reader
// @include        https://gamesmen.retailexpress.com.au/agent/*
// ==/UserScript==

var w = window;
var d = document;
var ls = localStorage;

var GMConfig = {
  developerMode : false,
  defaultBin : (ls.bin)?ls.bin:"",
  voice : (ls.voice)?ls.voice:"",
  voiceLocale : (ls.voiceLocale)?ls.voiceLocale:"en",
  voiceEnabled : (ls.voiceEnabled==="false")?false:true, //default to true
  rgx : {
    POS : /^([a-zA-Z0-9]+)$/,
    BIN : /^BIN-([a-zA-Z0-9\-\/]+)$/i,
    CMD : /^\/.+$/i,
    KEYCODE : /^[a-zA-Z0-9\s\-\/]{1}$/
  },
  binElement : '#inventoryDiv #E3',
  submitElement : '#mainDiv button[name=btnSubmit]',
  longDescElement : '#Long_Description',
  stockTableElement : '#Inventory_Grid_Frame form tbody',
  warningPopupMessage : '<div style="width:50%;background:white;padding:10px;text-align:center;font-size:2em;"><span style="color:red;font-size:2em;"><b>{0}</b></span><p>Replace <b>{1}</b> with <b>{2}</b> ?</p><p>Scan bin again to confirm.</p></div>',
  //sfxErrorData : 'https://soundbible.com/grab.php?id=1642&type=mp3',
  sfxReadyData : 'https://bitbucket.org/jordanlee93/rex-scripts/raw/master/ready.mp3',
  barcodeResetDelay : 2000
};

(function() {
  // ENTRY POINT
  var isInit = false;

  w.addEventListener('DOMContentLoaded', main);
  //w.addEventListener('load', main);
  var configMode = function() {
    document.body.innerHTML = `
    <form id='config'>
      <fieldset>
        <legend>Voice Synth</legend>
        <div><label>Enabled: </label><input class="config" type='checkbox' id='voiceenabled' ${GMConfig.voiceEnabled?'checked':''}/></div>
        <div><label>Voice: </label><input class="config" type='text' id='voice' value='${GMConfig.voice}'/></div>
        <div><label>Locale: </label><input class="config" type='text' id='locale' value='${GMConfig.voiceLocale}'/></div>
      </fieldset>
      <fieldset>
        <legend>Developer Mode</legend>
        <div><label>Enable Dev Mode: </label><input class="config" type='checkbox' id='devmode' ${GMConfig.developerMode?'checked':''}/></div>
        <div><label>Default Bin: </label><input class="config" type='text' id='defaultbin' value='${GMConfig.defaultBin}'/></div>
      </fieldset>
    </form>
    <button id="btnSaveConfigForm">Save Settings</button> <button onclick='window.location.reload()'>Exit</button>
    <p id='msg'></p>
    `;

    document.querySelector('#btnSaveConfigForm').addEventListener('click', function(e) {
      saveSettings(e);
    });
  }

  var saveSettings = function(e) {
    e.preventDefault();
    ls.voiceEnabled = document.forms["config"]["voiceenabled"].checked;
    ls.voice = document.forms["config"]["voice"].value;
    ls.voiceLocale = document.forms["config"]["locale"].value;
    ls.developerMode = document.forms["config"]["devmode"].checked;
    ls.defaultBin = document.forms["config"]["defaultbin"].value;
    return false;
  }

  var main = function() {
    var bindControls = function() {
      binText = d.querySelector(GMConfig.binElement);
      submitButton = d.querySelector(GMConfig.submitElement);

      var elements  = d.querySelectorAll('input:not(.config), textarea:not(.config)');
      for(var i = 0; i < elements.length; i++) {
        elements[i].readOnly = true;
      }

      if(binText && submitButton) return true;
      return false;
    };

    var trackingEvent = function(eventName) {
      if(sessionStorage.getItem("SessionID") == null) {
        sessionStorage.setItem("SessionID", (function(){
            let str = "";
            let bits = new Uint8Array(8);
            crypto.getRandomValues(bits);
            bits.forEach((c)=>str+=String.fromCharCode(c));
            return btoa(str);
          })()
        );
      }

      let trackingData = new FormData();
      trackingData.append("SessionID", sessionStorage.SessionID);
      trackingData.append("LoadTime", Date.now() - new Date(document.lastModified).getTime());
      trackingData.append("Event", eventName);
      try {
        trackingData.append("ProductID", document.querySelector('.sub-heading').textContent.match(/ID: ([0-9]{6})/)[0]);
      } catch (ex) {
        trackingData.append("ProductID", "Unknown");
      };
      try {
        trackingData.append("Barcode", document.querySelector("#Supplier_Product_ID").value);
      } catch (ex) {
        trackingData.append("Barcode", "Unknown");
      };
      try {
        trackingData.append("Bin", binText.value);
      } catch (ex) {
        trackingData.append("Bin", "Unknown");
      };
      fetch(
        new Request("https://staging.gamesmen.com.au/scan/rex_tracking.php",{
          method: 'POST',
          body: trackingData
        })
      );
    }

    isInit = true;

    trackingEvent("PageLoad");
    try {
      document.querySelector('#Form1').addEventListener('submit', e=>{
        bindControls();
        trackingEvent("SubmitPage");
      });
    } catch (ex) {
      trackingEvent("NoProduct");
    }

    try { new Audio(GMConfig.sfxReadyData).play(); } catch (ex) { console.log(ex); }

    var noProduct = !!(d.querySelector('.page-wrap').lastChild.textContent.match(/Product not found/ig));

    var productWebReady = (function(){try{ return d.querySelector('select[name="Attribute_Brand"]').value == 2} catch (ex) { return false; }})();

    var barcodeString = "";
    var oldBinCode = "";
    var overwriteCheck = false;

    var binText, submitButton = null;

    if(bindControls()) {
      trackingEvent("BindControlsSuccess");
    } else {
      trackingEvent("BindControlsFailed");
    }
    $(document).ajaxComplete(bindControls);

    var overlayContainer = d.createElement('div');
    overlayContainer.style = 'position:absolute;top:0;display:flex;width:100%;height:100%;align-items:center;justify-content:center;pointer-events:none;';
    d.body.appendChild(overlayContainer);
    var overlay = d.createElement('div');
    overlay.style = 'background:#000;color:#0f0;font-weight:bold;';
    overlayContainer.appendChild(overlay);

    var webReadyAlert = d.createElement('h1');
    webReadyAlert.textContent = 'PRODUCT NOT COMPLETE -- PASS TO ROSS';

    if(!noProduct && !productWebReady) d.querySelector('h1').parentNode.insertBefore(webReadyAlert, d.querySelector('h1').nextSibling);

    var warningPopup = d.createElement('div');
    warningPopup.show = function(msg, oldBinCode, newBinCode) {
      warningPopup.innerHTML = String.format(GMConfig.warningPopupMessage, msg, oldBinCode, newBinCode);
      try {
        // Prevent duplicates
        d.body.removeChild(warningPopup);
      } catch (ex) {
      } finally {
        d.body.appendChild(warningPopup);
      }
    };

    warningPopup.hide = function() {
      try { d.body.removeChild(warningPopup); } catch (ex) {};
    };
    warningPopup.style = 'display:flex;position:absolute;top:0;left:0;width:100%;height:100%;background:rgba(0,0,0,.5);justify-content:center;align-items:center;';
    warningPopup.innerHTML = GMConfig.warningPopupMessage;

    var writeBinHistory = function(binCode) {
      var dt = new Date();
      var msg = String.format("[{0}-{1}-{2} {3}:{4}] {5}\n", dt.getFullYear(), ((dt.getMonth()+1>9)?dt.getMonth()+1:'0'+(dt.getMonth()+1)), ((dt.getDate()>9)?dt.getDate():'0'+dt.getDate()), ((dt.getHours()>9)?dt.getHours():'0'+dt.getHours()), ((dt.getMinutes()>9)?dt.getMinutes():'0'+dt.getMinutes()), binCode);
      var textBox = d.querySelector(GMConfig.longDescElement);
      textBox.value = msg + textBox.value;
      textBox.value = textBox.value.substr(0,7999); // Enforce length limit
    };

    var barcodeReset = function(force) {
      if(force || !GMConfig.rgx.CMD.test(barcodeString)) {
        barcodeString = "";
        overlay.textContent = "Waiting for input...";
      }
    };

    var submitForm = function (binCode) {
      var runningOverlay = document.createElement('div');
      runningOverlay.innerHTML = '<div>Updating bin...</div>';
      runningOverlay.style = 'position:fixed; top:0; left:0; width:100%; height:100%; display:flex; align-items:center; justify-content:center; color:white; background:rgba(0,0,0,.5);';
      document.body.appendChild(runningOverlay);

      say("Bin updated to " + binCode);
      warningPopup.hide();
      overwriteCheck = false;
      writeBinHistory(binCode);
      binText.value = binCode.toUpperCase();
      barcodeString = "";

      if(!(submitButton instanceof HTMLButtonElement)) {
        bindControls();
      }
      if(binText.dispatchEvent(new Event('change'))) {
          setTimeout(()=>{
            trackingEvent("SubmitBinLocation");
            submitButton.dispatchEvent(new MouseEvent('click'))
          }, 3000);
      }
    };

    var hasStock = function () {
      var table = d.querySelector(GMConfig.stockTableElement);
      for(y=0;y<table.rows.length;y++) {
        if(table.rows[y].cells[1].textContent.trim() == 'Warehouse') {
          // Only checks AVAILABLE and ALLOCATED
          if( table.rows[y].cells[2].textContent.trim() !== "0" ||
              table.rows[y].cells[3].textContent.trim() !== "0") {
            return true;
          }
          return false;
        }
      }
    };

    var stockCount = function () {
      var table = d.querySelector(GMConfig.stockTableElement);
      for(y=0;y<table.rows.length;y++) {
        if(table.rows[y].cells[1].textContent.trim() == 'Warehouse') {
          // Only checks AVAILABLE and ALLOCATED
          return parseInt(table.rows[y].cells[2].textContent.trim()) + parseInt(table.rows[y].cells[3].textContent.trim());
        }
      }
    };

    var say = async function(text) {
      if(!GMConfig.voiceEnabled) return;
      setTimeout(async function() {
        var voices = await (async function(){
          return new Promise(
            function (resolve, reject) {
              let synth = window.speechSynthesis;
              let id;

              id = setInterval(() => {
                  if (synth.getVoices().length !== 0) {
                      resolve(synth.getVoices());
                      clearInterval(id);
                  }
              }, 10);
            }
          )
        })();
        var s = new SpeechSynthesisUtterance(text.replace(/\-/ig, ' '));
        s.lang = GMConfig.voiceLocale;
        for(i=0;i<voices.length-1;i++) {
          if(voices[i].name.toLowerCase().includes(GMConfig.voice.toLowerCase())) {
            s.voice = voices[i];
            break;
          }
        }
        s.rate = 1.5;
        w.speechSynthesis.speak(s);
      }, 0);
    }

    var timer = setInterval(barcodeReset, GMConfig.barcodeResetDelay);

    // DEVELOPER SCRIPTS
    if(!overwriteCheck && GMConfig.developerMode === true && typeof GMConfig.defaultBin === 'string') {
      if(binText) {
        if(binText.value == GMConfig.defaultBin) {
          warningPopup.show('READY TO SCAN', '', '');
          say("Ready to scan");
        } else if(binText.value == "" || binText.value == "NULL") {
          submitForm(GMConfig.defaultBin);
        } else if (binText.value !== "" && binText.value !== "NULL") {
          warningPopup.show('WARNING! OVERWRITE BIN CODE?', binText.value, GMConfig.defaultBin);
          say("Warning. Replacing " + binText.value + " with " + GMConfig.defaultBin + ". Are you sure?");
          overwriteCheck = true;
        } else {
          warningPopup.show('READY TO SCAN', '', '');
          say("Ready to scan");
        }
      }
    }

    var intro = setInterval(function(){if(noProduct) { say("Warning. No product found"); clearInterval(intro);}
    else {
      var plu;
      for(p in window.location.get) {if(p.toLowerCase() === "plu") {plu = window.location.get[p];}}
      if(typeof plu === 'undefined') {clearInterval(intro); return;}
      if(!bindControls()) return;
      if(ls.lastPLU !== plu && d.getElementById('Short_Description')) {
        var binSay = binText.value;
        var sub = [
          /* Bay special replacement */
          [/^(\d\d)-F-/g, "BAY $1, FRONT, "],
          [/^(\d\d)-M-/g, "BAY $1, MIDDLE, "],
          [/^(\d\d)-R-/g, "BAY $1, REAR, "],
          [/A$/g, "Alpha"],
          [/B$/g, "Bravo"],
          [/C$/g, "Charlie"],
          [/D$/g, "Delta"],
          [/E$/g, "Echo"],
          [/F$/g, "Foxtrot"],
          [/G$/g, "Golf"],
          [/H$/g, "Hotel"],
          [/I$/g, "India"],
          [/J$/g, "Juliette"],
          [/K$/g, "Kilo"],
          [/L$/g, "Lima"],
          [/M$/g, "Mike"],
          [/N$/g, "Novemberr"],
          [/O$/g, "Oscar"],
          [/P$/g, "Papa"],
          [/Q$/g, "Quebec"],
          [/R$/g, "Romeo"],
          [/S$/g, "Sierra"],
          [/T$/g, "Tango"],
          [/U$/g, "Uniform"],
          [/V$/g, "Victor"],
          [/W$/g, "Whiskey"],
          [/Y$/g, "Yankee"],
          [/Z$/g, "Zulu"]
        ];

        for(i=0;i<sub.length;binSay=binSay.replace(sub[i][0],sub[i++][1])){};

        binSay = 'Bin location ' + binSay;

        if (!hasStock()) say('No stock');
        else if((binText.value.match(/null/ig)) || binText.value == '') say('No bin.');
        else say(binSay + '. ' + stockCount() + ' in stock');
        ls.lastPLU = plu;
        clearInterval(intro);
      }
    }}, 16);

    w.addEventListener('keydown', function(evt){
      if(evt.key == "Enter") {
        if(GMConfig.rgx.BIN.test(barcodeString)) {
          say("Binning from Retail Express is no longer supported, please bin stock from Pick & Pack!");
          alert("Binning from Retail Express is no longer supported, please bin stock from Pick & Pack!");
          /*
          if(!(binText instanceof HTMLInputElement)) {
            bindControls();
          }
          // Strips prefix
          var shortBinCode = GMConfig.rgx.BIN.exec(barcodeString)[1];

          if(binText.value == "" || binText.value == "NULL" || !hasStock()) {
            // If bin text is empty/null or item has no stock, overwrite
            submitForm(shortBinCode);
          } else if(!overwriteCheck && binText.value != shortBinCode) {
            // If bin code is different to scanned bin code
            // show error message
            warningPopup.show('WARNING! OVERWRITE BIN CODE?', binText.value, shortBinCode);
            say("Warning. Replacing " + binText.value + " with " + shortBinCode + ". Are you sure?");
            overwriteCheck = true; // Checked on next run
            oldBinCode = shortBinCode;
            barcodeString = "";
          } else if (overwriteCheck && shortBinCode != oldBinCode && shortBinCode != binText.value) {
            // If detected overwrite AND confirmation scan doesn't match new bin code AND new bin code doesn't match existing bin code
            // show error message
            warningPopup.show('WARNING! OVERWRITE BIN CODE?', binText.value, shortBinCode);
            say("Warning. Replacing " + binText.value + " with " + shortBinCode + ". Are you sure?");
            oldBinCode = shortBinCode;
            barcodeString = "";
          } else if(overwriteCheck && shortBinCode == oldBinCode) {
            // Last scan detected existing bin code, this scan confirms by rescanning the same bin code
            submitForm(shortBinCode);
          } else {
            submitForm(shortBinCode);
          }
          */
        } else if(GMConfig.rgx.POS.test(barcodeString)) {

          if(overwriteCheck) {
            // CANCEL OVERWRITE
            overwriteCheck = false;
            warningPopup.hide();
          }

          w.location.href = w.location.origin + "/Admin/Inventory/ProductMaint/prodEdit.asp?PLU=" + barcodeString;
        } else if(GMConfig.rgx.CMD.test(barcodeString)) {
          var args = barcodeString.split(" ");
          switch(args[0].toLowerCase()) {
            case '/config':
              configMode();
              break;
            case '/voiceoff':
              ls.voiceEnabled = false;
              GMConfig.voiceEnabled = false;
              break;
            case '/voiceon':
              ls.voiceEnabled = true;
              GMConfig.voiceEnabled = true;
              break;
            case '/setvoice':
              ls.voice = barcodeString.split(/\s(.+)/)[1];
              break;
            case '/setlocale':
              ls.voiceLocale = barcodeString.split(/\s(.+)/)[1];
              break;
            default:
              console.warn('Bad command');
          }
          barcodeReset(true);
        }
      } else {
        clearInterval(timer);
        if(GMConfig.rgx.KEYCODE.test(evt.key)) {
          barcodeString += evt.key;
        }
        overlay.textContent = barcodeString;
        timer = setInterval(barcodeReset, GMConfig.barcodeResetDelay);
      }
    });

    return true;
  };


  if (!String.format) {
    String.format = function(format) {
      var args = Array.prototype.slice.call(arguments, 1);
      return format.replace(/{(\d+)}/g, function(match, number) {
        return typeof args[number] != 'undefined'
          ? args[number]
          : match
        ;
      });
    };
  }

  String.replaceBulk = function (str, findArray, replaceArray ){
    var i, regex = [], map = {};
    for( i=0; i<findArray.length; i++ ){
      regex.push( findArray[i].replace(/([-[\]{}()*+?.\\^$|#,])/g,'\\$1') );
      map[findArray[i]] = replaceArray[i];
    }
    regex = regex.join('|');
    str = str.replace( new RegExp( regex, 'g' ), function(matched){
      return map[matched];
    });
    return str;
  }

  if(!window.location.get) {
    var a = window.location.search.substr(1).split('&');
    if (a == "") window.location.get={};
    var b = {};
    for (var i = 0; i < a.length; ++i)
    {
      var p=a[i].split('=', 2);
      if (p.length == 1)
        b[p[0]] = "";
      else
        b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    window.location.get = b;
  }

  // Fallback for when the page loads before the event listener can be added
  var fallbackLoadTicker = setInterval(function() {
    if($.isReady && !isInit) {
      console.log('Fallback Loader Triggered');
      if(!!main()) clearInterval(fallbackLoadTicker);
    }
  }, 16);
})();
